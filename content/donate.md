---
title: "Donate"
date: 2024-04-04T14:11:40+05:30
draft: false
author: false
---

> **Razorpay** only accepts payments from Indian banks (Debit/Credit Cards and Net Banking) and apps (UPI, Mobile Wallets).
>
> If you are **outside India** and still want to support Prav, use **OpenCollective** instead.

## Why do we need donations?

At Prav, we do not have any corporate sponsors. We expect to get funded by our users buying subscriptions after the service is launched and through our shareholders investing initial amount. Donations can help us go extra mile as it gives people who believe in our philosophy to support  us without getting involved or downloading our app.

<div class="razorpay-embed-btn pulse" data-url="https://pages.razorpay.com/pl_NseNHjx2s9qd00/view" data-text="Donate Now!" data-color="#528FF0" data-size="large">
  <script>
    (function(){
      var d=document; var x=!d.getElementById('razorpay-embed-btn-js')
      if(x){ var s=d.createElement('script'); s.defer=!0;s.id='razorpay-embed-btn-js';
      s.src='https://cdn.razorpay.com/static/embed_btn/bundle.js';d.body.appendChild(s);} else{var rzp=window['__rzp__'];
      rzp && rzp.init && rzp.init()}})();
  </script>
</div>

## Donate to Prav IOS
You could also donate to support building Prav app for iPhone users.

<a href="https://opencollective.com/prav-ios" target="_blank">
  <img src="https://opencollective.com/webpack/donate/button@2x.png?color=blue" width=250 />
</a>
