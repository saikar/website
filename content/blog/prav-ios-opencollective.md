---
title: "We need your help to make Prav App available for iPhone users"
date: 2024-01-08T20:59:55+05:30
draft: false
author: false
---

We need your help! In our experience, [onboarding iOS users to XMPP is inconvenient right now](https://prav.app/blog/xmpp-apps-for-ios)
compared to Android - there is no Quicksy-like phone number/SMS OTP sign up
(With Quicksy app on Android, all you need to start chatting is to enter your
phone number similar to how popular apps like WhatsApp, Telegram or Signal
works). Developing a convenient and easy-to-onboard XMPP iOS app is one of
our top priorities.

Help us and the whole XMPP ecosystem by contributing to the Prav iOS app
fundraiser at the following link 👉 [https://opencollective.com/prav-ios](https://opencollective.com/prav-ios).
Prav iOS app will be Free/Libre Software following our track record of transparency and privacy.

The current status of this campaign as of 2024 Jan 19 is shown below,

<img src="/images/prav-ios-opencollective.png" alt="Prav iOS funding status" style="height:400px;">

Due to network effects, a messaging app is not just a personal choice and even
a single contact of yours having iOS can affect wider adoption of XMPP (we also
don't like Apple but we feel this is an essential step for adoption of XMPP by
people outside the Free Software circles). [Read more about it on our Azad Maidan post](https://azadmaidan.in/t/we-need-your-help-to-make-prav-app-available-for-iphone-users/232).

Remember that whatever be the amount, every donation counts!
