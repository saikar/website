---
title: "iOS app recommendations for XMPP"
date: 2022-03-11T00:54:07+05:30
draft: false
---
**Last updated: 9 January 2024**

We are [raising funds to build Prav iOS app based on Monal](https://opencollective.com/prav-ios). This can can take a while to complete, so in this post, we list XMPP apps currently available on iOS according to their features.

These are the features we consider essential:

- End-to-end encryption using OMEMO by default.
- Support background notifications.
- Ability for audio/video calls.
- Users can sign up within the app.
- Blocking accounts you don't want to chat with.
- Admins should have ability to delete messages in groups.

### Monal (Recommended)

[Monal](https://monal-im.org/) has all the essential features listed above. So, we recommend you use Monal for XMPP in iOS.

Here are some screenshots of Monal

<img src="/images/monal_1.png" alt="Monal Welcome Screen" style="height:400px;">
<img src="/images/monal_2.png" alt="Monal Registration Screen" style="height:400px;">
<img src="/images/monal_3.png" alt="Monal chat" style="height:400px;">

### Snikket (If you already have account)

[Snikket](https://snikket.org) has all the essential features listed above in this post, expect that it does not have sign up option in the app. If you already have an XMPP account, then Snikket is a good choice. But we don't recommend it for newbies who do not have an account. 

<img src="/images/snikket.png" alt="Snikket app" style="height:400px;">

### Siskin IM (Not recommended)

Siskin IM needs many tweaks before it can be recommended. The problems we found were: 

1. Services suggested by default needs email confirmation but it is not obvious from the app interface.

2. HTTP upload needs to be enabled by default for asynchronous file sharing. Default is jigle file transfer, which is peer to peer but both users need to be online to transfer files.

3. Subscription needs to be enabled manually after adding a contact. This is sometimes required for omemo encryption.

4. OMEMO needs to be enabled for each chat/contact from settings.

5. Notifications don't work if the app is not open.

<img src="/images/siskin.jpg" alt="Siskin app" style="height:400px;">
<br>
<img src="/images/siskin_1.jpg" alt="Siskin app" style="height:400px;">

**Note 1: 
In iOS, [notifications work differently](https://www.freie-messenger.de/en/xmpp/apple/#notifications) from Android. If the chat app is unused for a long time, iOS shuts down its bckground process to reduce power consumption, which makes it difficult to receive notifications when the app is unused for a long time.**

**Note 2:
If you'd like to see a Prav iOS app soon, please donate at [our open collective campaign hosted by XMPP Foundation](https://opencollective.com/prav-ios).**
